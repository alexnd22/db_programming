import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Departament, Angajat, Bonus

CONNECTION_STRING = 'mysql+pymysql://{user}:{password}@{host}/{db}'
eng = create_engine(CONNECTION_STRING.format(user='root', password='36977ebl', host='localhost', db='employees_db'))

Base.metadata.create_all(eng)

Session = sessionmaker(bind=eng)
s = Session()

# s.add_all(
#     [
#         Departament(nume='Marketing'),
#         Departament(nume='HR'),
#         Departament(nume='Contabilitate'),
#         Departament(nume='IT')
#     ]
# )
#
# s.commit()
#
# s.add_all(
#     [
#         Angajat(cnp=123456, nume='Martin', prenume='Alin', data_angajare=datetime.date(2020, 4, 11),
#                 departament=1, salariu=4000, manager=False),
#         Angajat(cnp=123444, nume='Ionescu', prenume='Ionela', data_angajare=datetime.date(2018, 5, 16),
#                 departament=2, salariu=3500, manager=False),
#         Angajat(cnp=123444, nume='Teodora', prenume='Gheorghe', data_angajare=datetime.date(2019, 5, 22),
#                 departament=3, salariu=6600, manager=True),
#     ]
# )
#
# s.commit()
#
# s.add_all(
#     [
#         Bonus(data_acordarii=datetime.date(2021, 12, 31), beneficiar=3, valoarea=1300),
#         Bonus(data_acordarii=datetime.date(2021, 12, 31), beneficiar=2, valoarea=1200),
#         Bonus(data_acordarii=datetime.date(2021, 12, 31), beneficiar=1, valoarea=1100),
#     ]
# )
#
# s.commit()

# salariu_max = s.query(Angajat).filter(Angajat.salariu).order_by(Angajat.salariu.desc()).limit(1)
salariu_max = s.query(Angajat).filter(Angajat.salariu).order_by(Angajat.salariu.desc()).first()
print(salariu_max)


def add_employee():
    cnp = input('CNP: ')
    nume = input('Nume: ')
    prenume = input('Prenume: ')
    data_angajare = datetime.datetime.now().date()
    departament = input('Departament ID: ')
    salariu = input('Salariu: ')
    manager = int(input('Manager 1 = DA, 0 = NU: '))
    s.add(
        Angajat(cnp=cnp, nume=nume, prenume=prenume, data_angajare=data_angajare,
                departament=departament, salariu=salariu, manager=manager)
    )
    s.commit()


# add_employee()

def assign_employee(id_angajat, id_departament, is_manager):
    angajat_curent = s.query(Angajat).filter(Angajat.id == id_angajat).first()
    angajat_curent.departament = id_departament
    angajat_curent.manager = is_manager
    s.commit()


# assign_employee(2, 2, 0)

angajat_curent = s.query(Angajat).filter(Angajat.id == 2).all()
print(angajat_curent[0])
print(type(angajat_curent))
