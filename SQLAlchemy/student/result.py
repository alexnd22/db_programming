'''
SQL Alchemy este un ORM - OBJECT RELATIONAL MAPPING - o reprezentare a obiectelor in python sub o alta forma (cel mai popular)
- nu foloseste sintaxa de SQL ci doar de Python
- clasa reprezinta un tabel
- o instanta reprezinta un rand
- atributele reprezinta coloanele

SQL Alchemy sessionmaker() ce reprezinta comunicarea si manipularea cu baze de date care este reprezentata de un cursor IN PyMySql
pip install sqlalchemy
pip install pymysql

Tabelele sunt reprezentate de clase, clasele pentru a putea fi luate in considerare trebuie sa mosteneasca declarative_base()
'''
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Base, Student

CONNECTION_STRING = 'mysql+pymysql://{user}:{password}@{host}/{db}'
eng = create_engine(CONNECTION_STRING.format(user='root', password='36977ebl', host='localhost', db='students_db'))
# create_engine realizeaza o conexiune catre baza de date (students_db)

# creaza tabelele in baza noastra de date
Base.metadata.create_all(eng)

Session = sessionmaker(bind=eng)  # ne da accesul la sesiune
s = Session()

# s.add_all(
#     [
#         Student(first_name='Alex', last_name='Oprea'),
#         Student(first_name='Cristina', last_name='Neculae'),
#         Student(first_name='Horia', last_name='Brenciu')
#     ]
# )
# s.commit()  # commit() la fiecare modificare de DB(insert/update/delete)

# SELECT * FROM students
all_students = s.query(Student).all()
for student in all_students:
    print(student.first_name, student.last_name)

# SELECT COUNT(*) FROM students
count_students = s.query(Student).count()
print(f'Total: {count_students}')

# SELECT first_name, last_name FROM students;
all_students = s.query(Student.first_name, Student.last_name).all()
print(all_students)
print('----------------------')
# SELECT * from students WHERE id>=2 AND first_name LIKE 'Al%';
results = s.query(Student).filter(Student.student_id >= 1, Student.first_name.like('Al%')).all()
for result in results:
    print(result)
