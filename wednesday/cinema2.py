from dateutil.parser import parse
from datetime import datetime
import mysql.connector as mysql


def create_structure():
    conn = mysql.connect(host='localhost', user='root', password='36977ebl')

    with conn.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS cinema;')
        c.execute('CREATE TABLE IF NOT EXISTS cinema.clienti ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'nume VARCHAR(50) NOT NULL,'
                  'email VARCHAR(30) NOT NULL,'
                  'telefon VARCHAR(15) NOT NULL,'
                  'data_nasterii TIMESTAMP);')
        c.execute('CREATE TABLE IF NOT EXISTS cinema.filme ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'nume VARCHAR(50) NOT NULL,'
                  'an_lansare INTEGER NOT NULL,'
                  'descriere TEXT NOT NULL);')
        c.execute('CREATE TABLE IF NOT EXISTS cinema.sali ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'denumire VARCHAR(20) NOT NULL,'
                  'capacitate INTEGER NOT NULL,'
                  'vip BOOLEAN NOT NULL DEFAULT 0);')
        c.execute('CREATE TABLE IF NOT EXISTS cinema.bilete ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'film_id INTEGER NOT NULL,'
                  'sala_id INTEGER NOT NULL,'
                  'client_id INTEGER NOT NULL,'
                  'pret DECIMAL NOT NULL,'
                  'data DATETIME NOT NULL,'
                  'CONSTRAINT fk_film_id FOREIGN KEY (film_id) REFERENCES filme(id),'
                  'CONSTRAINT fk_sala_id FOREIGN KEY (sala_id) REFERENCES sali(id),'
                  'CONSTRAINT fk_client_id FOREIGN KEY (client_id) REFERENCES clienti(id));')
    conn.close()


create_structure()

conn = mysql.connect(host='localhost', user='root', password='36977ebl', database='cinema')


def populare1():
    with conn.cursor() as c:
        c.execute(f'''INSERT INTO clienti (nume, email, telefon, data_nasterii)
                     VALUES ('Alex', 'mail1', '072', '2021-12-12 12:00:00') ;''')
        conn.commit()


def populare2():
    with conn.cursor() as c:
        c.execute('''INSERT INTO filme (nume, an_lansare, descriere) 
                    VALUES ('Hulk', 2016, 'Omu verde');''')
        c.execute('''INSERT INTO sali (denumire, capacitate, vip) 
                    VALUES ('Sala 3', 100, True);''')
        conn.commit()


def populare3():
    with conn.cursor() as c:
        c.execute(F'''INSERT INTO bilete (film_id, sala_id, client_id,data, pret) 
                    VALUES (1, 1, 1,'2020-02-03 12:00:00', 30)''')
        conn.commit()


# populare1()
# populare2()
# populare3()


def calculare_pret_per_an():
    year = int(input('Enter movie year: '))
    with conn.cursor() as c:
        c.execute('SELECT SUM(pret) from bilete where film_id=1 and YEAR(data)=%s;', (year,))
        result = c.fetchone()
        print(f'Incasare de {result[0]}')


def incasari_mici():
    with conn.cursor() as c:
        c.execute('SELECT sala_id,  SUM(pret) as incasari FROM bilete GROUP BY sala_id ORDER BY incasari LIMIT 1')
        result = c.fetchone()
        print(result)
        print(f'Sala cu cele mai mici incasari: sala {result[0]} si incasari {result[1]} lei')


incasari_mici()


