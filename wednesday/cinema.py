from dateutil.parser import parse
from datetime import datetime
import mysql.connector as mysql


def create_structure():
    conn = mysql.connect(host='localhost', user='root', password='36977ebl')

    with conn.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS cinema')
        c.execute('''CREATE TABLE IF NOT EXISTS cinema.clients (
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    name VARCHAR(25) NOT NULL,
                    email VARCHAR(30) NOT NULL,
                    phone_no VARCHAR(20) NOT NULL,
                    date_of_birth TIMESTAMP NOT NULL                                        
                    );
                    '''
                  )
        c.execute('''CREATE TABLE IF NOT EXISTS cinema.movies (
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    name VARCHAR(35) NOT NULL,
                    launching_year INT NOT NULL,
                    description VARCHAR(255)
                    );
                    '''
                  )

        c.execute('''CREATE TABLE IF NOT EXISTS cinema.rooms (
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    name VARCHAR(35) NOT NULL,
                    capacity INT NOT NULL,
                    VIP BOOLEAN NOT NULL DEFAULT 0
                    );
                    '''
                  )

        c.execute('''CREATE TABLE IF NOT EXISTS cinema.tickets(
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    CONSTRAINT fk_movies_id FOREIGN KEY (movies_id) REFERENCES movies(id),
                    CONSTRAINT fk_rooms_id FOREIGN KEY (rooms_id) REFERENCES rooms(id),
                    CONSTRAINT fk_clients_id FOREIGN KEY (clients_id) REFERENCES clients(id),
                    price BOOLEAN NOT NULL
                    );
                    '''
                  )

    conn.close()


create_structure()
