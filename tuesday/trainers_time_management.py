from dateutil.parser import parse
from datetime import datetime
import mysql.connector as mysql


def create_structure():
    conn = mysql.connect(host='localhost', user='root', password='36977ebl', database='')

    # cursor = conn.cursor()
    with conn.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS trainers')
        c.execute('''CREATE TABLE IF NOT EXISTS trainers.trainer (
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    name VARCHAR(25) NOT NULL,
                    email VARCHAR(30) NOT NULL,
                    location VARCHAR(20) NOT NULL,
                    contract VARCHAR(40) NOT NULL,
                    password TEXT NOT NULL
                    );
                    '''
                  )
        c.execute('''CREATE TABLE IF NOT EXISTS trainers.session (
                    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                    number_of_hours integer NOT NULL,
                    start_date TIMESTAMP NOT NULL,
                    stop_date TIMESTAMP NOT NULL,
                    order_number VARCHAR(20) NOT NULL,
                    base_price DECIMAL(10.0) NOT NULL,
                    group_id TEXT NOT NULL,
                    is_paid BOOLEAN NOT NULL DEFAULT 0,
                    trainer_id INT NOT NULL,
                    CONSTRAINT fk_trainer_id FOREIGN KEY (trainer_id) REFERENCES trainer(id)
                    );
                    '''
                  )

    conn.close()


def show_menu1():
    print('-' * 22)
    print('1. Login')
    print('2. Register')
    print('3. Exit')


def show_menu2():
    print('-' * 22)
    print('1. Add new training session')
    print('2. Get upcoming training sessions')
    print('3. Get unpaid training sessions')
    print('4. Generate bill')
    print('5. Get total paid sum in a given year')
    print('6. Logout')
    print('7. Exit')


create_structure()

conn = mysql.connect(host='localhost', user='root', password='36977ebl', database='trainers')

user_logat = None
while True:
    if user_logat is None:
        show_menu1()
        option = int(input('Option = '))

        if option == 3:
            break
        elif option == 1:
            email = input('Enter your email: ')
            password = input('Enter your password: ')
            with conn.cursor() as c:
                c.execute('SELECT id FROM trainer WHERE email=%s AND password=%s;', (email, password))

                result = c.fetchone()  # metoda returneaza o singura inregistrare sau nici una

                if result:
                    user_logat = result[0]
                else:
                    print('Email address or email is wrong')
        elif option == 2:
            name = input('Enter your name: ')
            email = input('Enter your email: ')
            location = input('Enter your location: ')
            contract = input('Enter your contract number: ')
            password = input('Enter your password: ')
            with conn.cursor() as c:
                c.execute(
                    "INSERT INTO trainer (name, email, location, contract, password)  VALUES (%s, %s, %s, %s, %s );",
                    (name, email, location, contract, password))
                conn.commit()

    else:
        show_menu2()
        option = int(input('Enter your option: '))
        if option == 7:
            break
        elif option == 6:
            user_logat = None
        elif option == 1:
            number_of_hours = int(input('Enter hours: '))
            start_date = parse(input('Enter start date: '))
            stop_date = parse(input('Enter stop date: '))
            order_number = input('Enter order no.: ')
            base_price = float(input('Base price is: '))
            group_id = input('Enter your group: ')
            with conn.cursor() as c:
                c.execute(
                    "INSERT INTO session "
                    "(number_of_hours, start_date, stop_date, order_number, base_price, group_id, trainer_id)"
                    "VALUES (%s, %s, %s, %s, %s, %s, %s );",
                    (number_of_hours, start_date, stop_date, order_number, base_price, group_id, user_logat))
                conn.commit()

        elif option == 2:
            with conn.cursor() as c:
                c.execute('SELECT * FROM session WHERE start_date > %s AND trainer_id = %s;', datetime.now(),
                          user_logat)
                results = c.fetchall()
                for result in results:
                    print(result)
        elif option == 3:
            with conn.cursor() as c:
                c.execute('SELECT * FROM session WHERE is_paid=0 AND trainer_id=%s', (user_logat,))
                results = c.fetchall()
                for result in results:
                    print(f'Your unpaid sessions are: ID ->{result[0]} Number of Hours->{result[1]} '
                          f'Start day->{result[2]} End day->{result[3]} Order no.->{result[4]} '
                          f'Base price->{result[5]} Group Id->{result[6]} Is Paid->{result[7]} '
                          f'Trainer Id->{result[8]}')
        elif option == 4:
            with conn.cursor() as c:
                c.execute('SELECT * FROM session WHERE is_paid=0 AND trainer_id = %s; ', (user_logat,))
                results = c.fetchall()
                for result in results:
                    c.execute('UPDATE session SET is_paid=1 WHERE id= %s; ', (result[0],))
                conn.commit()

        elif option == 5:
            year = int(input('Enter year: '))
            with conn.cursor() as c:
                c.execute('''SELECT SUM(number_of_hours * base_price) FROM session WHERE is_paid = 1
                            AND trainer_id=%s AND start_date BETWEEN %s AND %s AND stop_date BETWEEN %s AND %s;
                            ''', (user_logat, datetime(year, 1, 1, 0, 0),
                                  datetime(year, 12, 31, 23, 59, 59), datetime(year, 1, 1, 0, 0),
                                  datetime(year, 12, 31, 23, 59, 59)))
                result = c.fetchone()
                print(f'Am incasat in anul {year} suma de {result[0]} EUR')

conn.close()
