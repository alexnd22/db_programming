import mysql.connector as mysql

db = mysql.connect(host='localhost', user='root', password='36977ebl')
cursor = db.cursor()
cursor.execute('SHOW DATABASES')

# fetchall() returns a list of all databases present
databases = cursor.fetchall()

for database in databases:
    print(database)
