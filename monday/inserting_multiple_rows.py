import mysql.connector as mysql

db = mysql.connect(host='localhost', user='root', password='36977ebl', database='DBPython')
cursor = db.cursor()
# define the query
query = 'INSERT INTO users (name, user_name) VALUES (%s, %s)'

values = [
    ('Alex', 'user_alex'),
    ('Alin', 'user_alin'),
    ('Adrian', 'user_adrian')
]
# executing the query with values
cursor.executemany(query, values)

db.commit()

print(cursor.rowcount, 'records inserted')

