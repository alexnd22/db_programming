import mysql.connector as mysql

db = mysql.connect(host='localhost', user='root', password='36977ebl', database='DBPython')
cursor = db.cursor()
# stergem tabela users din baza noastra de date 'DBPython'
cursor.execute('DROP TABLE users')
cursor.execute(
    'CREATE TABLE users(id INTEGER(11) AUTO_INCREMENT PRIMARY KEY NOT NULL, '
    'name VARCHAR(255), user_name VARCHAR(255))')

# dropping the id column
cursor.execute('ALTER TABLE users DROP id')

# adding id column to table 'users'
# 'first' keyword in the statement will add a column in the starting of the table
cursor.execute('ALTER TABLE users ADD COLUMN id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST')

cursor.execute('DESC users')

print(cursor.fetchall())
