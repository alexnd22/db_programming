import mysql.connector as mysql

db = mysql.connect(host='localhost', user='root', password='36977ebl')


def create_db():
    with db.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS db_tasks')
        c.close()


def create_table():
    with db.cursor() as c:
        c.execute('CREATE TABLE IF NOT EXISTS db_tasks.tasks ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'task TEXT NOT NULL,'
                  'done BOOLEAN DEFAULT 0)')
        c.close()


def show_task_list():
    with db.cursor() as c:
        c.execute('SELECT * FROM tasks WHERE NOT done')
        results = c.fetchall()
        for result in results:
            print(result)
        print('-' * 20)


def add_task():
    add_task = input('Introdu taskul: ')
    with db.cursor() as c:
        c.execute('''INSERT INTO tasks (task) VALUES (%s)''', (add_task,))
        print('Task adaugat!')


def mark_as_done():
    task_id = input('Introdu id-ul taskului: ')
    with db.cursor() as c:
        c.execute('UPDATE tasks SET done=1 WHERE id=%s AND done=0', (task_id,))
        db.commit()


def menu():
    print('1. Add task.')
    print('2. Show task list.')
    print('3. Mark a task as DONE.')
    print('4. Exit')


def run_app():
    while True:
        menu()
        option = int(input('Ke vrei sa faki?!: '))
        if option == 1:
            add_task()
        if option == 2:
            show_task_list()
        if option == 3:
            mark_as_done()
        if option == 4:
            print('App closed! Go home!')
            break


create_db()
db = mysql.connect(host='localhost', user='root', password='36977ebl', database='db_tasks')
create_table()
run_app()
